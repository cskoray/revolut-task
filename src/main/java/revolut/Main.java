package revolut;

import org.jooby.Jooby;
import org.jooby.json.Jackson;
import revolut.controller.AccountController;
import revolut.domain.Account;
import revolut.service.AccountTransactionService;
import revolut.service.CurrencyRateSupplier;
import revolut.service.InMemoryIAccountRepository;
import revolut.service.Interface.IAccountRepository;
import revolut.service.Interface.IAccountTransactionService;
import revolut.service.Interface.ICurrencyRateSupplier;
import revolut.service.Interface.ITransactionService;
import revolut.service.TransactionService;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;
import static revolut.domain.Currency.*;

public class Main extends Jooby {
    {
        use(new Jackson());
        use((env, conf, binder) -> {
            binder.bind(IAccountRepository.class).to(InMemoryIAccountRepository.class);
            binder.bind(IAccountTransactionService.class).to(AccountTransactionService.class);
            binder.bind(ICurrencyRateSupplier.class).to(CurrencyRateSupplier.class);
            binder.bind(ITransactionService.class).to(TransactionService.class);
        });
        use(AccountController.class);

        onStart((registry) -> {
            IAccountRepository accountRepository = registry.require(IAccountRepository.class);

            Account eurAccount = new Account("acc1", EUR, new BigDecimal(100.1239).setScale(2, HALF_UP));
            Account usdAccount = new Account("acc2", USD, new BigDecimal(200.2888).setScale(2, HALF_UP));
            Account trAccount = new Account("acc3", TRY, new BigDecimal(300.129).setScale(2, HALF_UP));
            accountRepository.saveAccount(eurAccount);
            accountRepository.saveAccount(usdAccount);
            accountRepository.saveAccount(trAccount);
        });
    }

    public static void main(final String[] args) {
        run(Main::new, args);
    }
}
