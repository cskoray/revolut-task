package revolut.controller;

import org.jooby.Err;
import org.jooby.Result;
import org.jooby.mvc.Body;
import org.jooby.mvc.GET;
import org.jooby.mvc.POST;
import org.jooby.mvc.Path;
import revolut.service.Interface.IAccountRepository;
import revolut.service.Interface.ITransactionService;

import javax.inject.Inject;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static org.jooby.Results.ok;

@Path("/accounts")
public class AccountController {

    private final ITransactionService transactionService;
    private final IAccountRepository accountRepository;

    @Inject
    public AccountController(ITransactionService transactionService, IAccountRepository accountRepository) {
        this.transactionService = transactionService;
        this.accountRepository = accountRepository;
    }

    @GET
    public List<AccountDto> accountList() {
        return accountRepository.getAccounts().stream()
                .map(AccountDto::accountData)
                .sorted(comparing(AccountDto::getAccountNum))
                .collect(toList());
    }

    @Path("/{accountnum}")
    @GET
    public AccountDto getAccount(String accountnum) {
        return accountRepository.getAccount(accountnum)
                .map(AccountDto::accountData)
                .orElseThrow(() -> new Err(404));
    }

    @Path("/transaction")
    @POST
    public Result makeTransaction(@Body TransactionRequest transactionRequest) {
        transactionService.makeTransaction(transactionRequest.getAccountFrom(), transactionRequest.getAccountTo(), transactionRequest.getAmount());
        return ok();
    }
}
