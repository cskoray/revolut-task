package revolut.controller;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import revolut.domain.Account;

import java.math.BigDecimal;

@JsonPropertyOrder({"accountNum", "currency", "accountBalance"})
public class AccountDto {

    private String accountNum;
    private String currency;
    private BigDecimal accountBalance;

    public AccountDto(String accountNum, String currency, BigDecimal accountBalance) {
        this.accountNum = accountNum;
        this.currency = currency;
        this.accountBalance = accountBalance;
    }

    public static AccountDto accountData(Account account) {
        return new AccountDto(account.getAccountNum(), account.getCurrency().getCode(), account.getAccountBalance());
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }
}
