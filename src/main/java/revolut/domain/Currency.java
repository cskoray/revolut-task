package revolut.domain;

public enum Currency {

    EUR("EUR"),
    USD("USD"),
    TRY("TRY");

    private final String code;

    Currency(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
