package revolut.service;

import revolut.domain.Account;
import revolut.service.Interface.IAccountTransactionService;

import javax.inject.Singleton;
import java.util.function.BiConsumer;

import static java.lang.Long.parseLong;

@Singleton
public class AccountTransactionService implements IAccountTransactionService {

    @Override
    public void transaction(Account fromAccount, Account toAccount, BiConsumer<Account, Account> biConsumer) {

        Long fromAccNum = parseLong(fromAccount.getAccountNum().replace("acc", ""));
        Long toAccNum = parseLong(toAccount.getAccountNum().replace("acc", ""));

        if (fromAccNum < toAccNum) {
            acquireAccountLock(fromAccount, toAccount, biConsumer);
        } else {
            acquireAccountLock(toAccount, fromAccount, biConsumer);
        }
    }

    private void acquireAccountLock(Account sender, Account receiver, BiConsumer<Account, Account> biConsumer) {
        sender.lock();
        try {
            receiver.lock();
            try {
                biConsumer.accept(sender, receiver);
            } finally {
                receiver.unlock();
            }
        } finally {
            sender.unlock();
        }
    }
}
