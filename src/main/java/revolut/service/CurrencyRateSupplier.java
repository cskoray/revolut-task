package revolut.service;

import revolut.domain.Currency;
import revolut.service.Interface.ICurrencyRateSupplier;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static java.math.BigDecimal.ONE;

@Singleton
public class CurrencyRateSupplier implements ICurrencyRateSupplier {
    private static final Map<Exchange, BigDecimal> exchangeOffice = new HashMap<>();

    static {
        exchangeOffice.put(new Exchange("TRY", "USD"), new BigDecimal("0.22"));
        exchangeOffice.put(new Exchange("USD", "TRY"), new BigDecimal("4.59"));

        exchangeOffice.put(new Exchange("TRY", "EUR"), new BigDecimal("0.19"));
        exchangeOffice.put(new Exchange("EUR", "TRY"), new BigDecimal("5.30"));

        exchangeOffice.put(new Exchange("EUR", "USD"), new BigDecimal("1.15"));
        exchangeOffice.put(new Exchange("USD", "EUR"), new BigDecimal("0.87"));
    }

    @Override
    public BigDecimal getRate(Currency from, Currency to) {
        if (from.equals(to)) {
            return ONE;
        }
        Exchange key = new Exchange(from.getCode(), to.getCode());
        BigDecimal rate = exchangeOffice.get(key);
        if (rate == null) {
            throw new IllegalArgumentException(from.getCode() + "-" + to.getCode() + " rate does not exist");
        }
        return rate;
    }

    private static class Exchange {
        String curFrom;
        String curTo;

        public Exchange(String curFrom, String curTo) {
            this.curFrom = curFrom;
            this.curTo = curTo;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Exchange) {
                Exchange exchange = (Exchange) obj;
                return (exchange.curFrom.equals(this.curFrom) && exchange.curTo == this.curTo);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return (int) curFrom.hashCode() * curTo.hashCode();
        }

        public String getCurFrom() {
            return curFrom;
        }

        public void setCurFrom(String curFrom) {
            this.curFrom = curFrom;
        }

        public String getCurTo() {
            return curTo;
        }

        public void setCurTo(String curTo) {
            this.curTo = curTo;
        }

        @Override
        public String toString() {
            return getCurFrom() + "-" + getCurTo();
        }
    }

    public Map<Exchange, BigDecimal> getExchangeOffice() {
        return exchangeOffice;
    }
}
