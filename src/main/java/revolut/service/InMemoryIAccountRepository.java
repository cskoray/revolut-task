package revolut.service;

import revolut.domain.Account;
import revolut.service.Interface.IAccountRepository;

import javax.inject.Singleton;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Optional.ofNullable;

@Singleton
public class InMemoryIAccountRepository implements IAccountRepository {

    private final Map<String, Account> accountDb = new ConcurrentHashMap<>();

    @Override
    public Collection<Account> getAccounts() {
        return accountDb.values();
    }

    @Override
    public Optional<Account> getAccount(String accountNum) {
        return ofNullable(accountDb.get(accountNum));
    }

    @Override
    public void saveAccount(Account account) {
        accountDb.put(account.getAccountNum(), account);
    }
}
