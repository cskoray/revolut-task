package revolut.service.Interface;

import revolut.domain.Account;

import java.util.Collection;
import java.util.Optional;

public interface IAccountRepository {

    Collection<Account> getAccounts();

    Optional<Account> getAccount(String accountNum);

    void saveAccount(Account account);
}
