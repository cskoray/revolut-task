package revolut.service.Interface;

import revolut.domain.Account;

import java.util.function.BiConsumer;

public interface IAccountTransactionService {

    void transaction(Account fromAccount, Account toAccount, BiConsumer<Account, Account> biConsumer);
}
