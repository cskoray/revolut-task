package revolut.service.Interface;

import revolut.domain.Currency;

import java.math.BigDecimal;

public interface ICurrencyRateSupplier {

    BigDecimal getRate(Currency from, Currency to);
}
