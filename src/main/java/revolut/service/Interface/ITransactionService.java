package revolut.service.Interface;

import java.math.BigDecimal;

public interface ITransactionService {

    void makeTransaction(String fromAccountNum, String toAccountNum, BigDecimal amount);
}
