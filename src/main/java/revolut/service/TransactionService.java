package revolut.service;

import revolut.domain.Account;
import revolut.service.Interface.IAccountRepository;
import revolut.service.Interface.IAccountTransactionService;
import revolut.service.Interface.ICurrencyRateSupplier;
import revolut.service.Interface.ITransactionService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;

@Singleton
public class TransactionService implements ITransactionService {

    private final IAccountRepository accountRepository;
    private final ICurrencyRateSupplier rateSupplier;
    private final IAccountTransactionService transactionService;

    @Inject
    public TransactionService(IAccountRepository accountRepository, ICurrencyRateSupplier rateSupplier, IAccountTransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.rateSupplier = rateSupplier;
        this.transactionService = transactionService;
    }

    @Override
    public void makeTransaction(String fromAccountNum, String toAccountNum, BigDecimal amount) {

        if (amount.compareTo(ZERO) <= 0)
            throw new IllegalArgumentException("Transaction amount cannot be negative or zero");

        Account fromAcc = getAccount(fromAccountNum);
        Account toAcc = getAccount(toAccountNum);

        BigDecimal rate = rateSupplier.getRate(fromAcc.getCurrency(), toAcc.getCurrency());
        BigDecimal ratedAmount = amount.multiply(rate);

        transactionService.transaction(fromAcc, toAcc, (from, to) -> {
            if (from.getAccountBalance().compareTo(amount) < 0) {
                throw new IllegalArgumentException("Not enough balance for " + fromAccountNum);
            }
            from.setAccountBalance(from.getAccountBalance().subtract(amount).setScale(2, HALF_UP));
            to.setAccountBalance(to.getAccountBalance().add(ratedAmount).setScale(2, HALF_UP));
        });
    }

    private Account getAccount(String accountNum) {
        return accountRepository.getAccount(accountNum).orElseThrow(IllegalArgumentException::new);
    }

    public IAccountRepository getAccountRepository() {
        return accountRepository;
    }

    public ICurrencyRateSupplier getRateSupplier() {
        return rateSupplier;
    }

    public IAccountTransactionService getTransactionService() {
        return transactionService;
    }
}
