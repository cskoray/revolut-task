package revolut;

import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.jooby.test.JoobyRule;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import revolut.controller.TransactionRequest;
import revolut.domain.Account;
import revolut.domain.Currency;
import revolut.service.Interface.IAccountRepository;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.path.json.config.JsonPathConfig.NumberReturnType.BIG_DECIMAL;
import static java.math.BigDecimal.ROUND_HALF_UP;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static revolut.domain.Currency.EUR;
import static revolut.domain.Currency.TRY;

public class AccountControllerTest {

    private static Main main = new Main();

    @ClassRule
    public static JoobyRule rule = new JoobyRule(main);

    @BeforeClass
    public static void config() {
        RestAssured.config = RestAssured.config().jsonConfig(jsonConfig().numberReturnType(BIG_DECIMAL));
    }

    @Test
    public void testAccountListSize() {
        createMockAccount("acc1", new BigDecimal("0.00"), TRY);
        createMockAccount("acc2", new BigDecimal("1.00"), TRY);
        createMockAccount("acc3", new BigDecimal("2.00"), TRY);
        createMockAccount("acc4", new BigDecimal("2.00"), TRY);
        ValidatableResponse response = given().when().get("/accounts").then();
        response.statusCode(200);
        assertEquals(response.extract().jsonPath().getList("$").size(), 4);
    }

    @Test
    public void testInvalidUrl() {
        when()
                .get("/iAmInvalid")
                .then()
                .statusCode(404);
    }

    @Test
    public void testGetAccount() {
        createMockAccount("acc1", new BigDecimal("1.00").setScale(2, ROUND_HALF_UP), TRY);
        createMockAccount("acc2", new BigDecimal("20.00").setScale(2, ROUND_HALF_UP), EUR);
        when()
                .get("/accounts/acc1")
                .then()
                .body("accountNum", is("acc1"))
                .body("accountBalance", comparesEqualTo(new BigDecimal("1.00")));
        when()
                .get("/accounts/acc2")
                .then()
                .body("accountNum", is("acc2"))
                .body("accountBalance", comparesEqualTo(new BigDecimal(20.00)));
    }

    @Test
    public void testGetMissingAccount() {
        createMockAccount("acc1", new BigDecimal("2.00"), TRY);
        when()
                .get("/accounts/acc11111")
                .then()
                .statusCode(404);
    }

    @Test
    public void testValidTransaction() {
        createMockAccount("acc1", new BigDecimal("20.00"), TRY);
        createMockAccount("acc2", new BigDecimal("10.00"), TRY);
        given().
                when()
                .body(new TransactionRequest("acc2", "acc1", new BigDecimal("2.00")))
                .contentType("application/json")
                .post("/accounts/transaction")
                .then().statusCode(200);
        when()
                .get("/accounts/acc1")
                .then()
                .body("accountBalance", comparesEqualTo(new BigDecimal("22.00")));
        when()
                .get("/accounts/acc2")
                .then()
                .body("accountBalance", comparesEqualTo(new BigDecimal("8.00")));
    }

    @Test
    public void testValidTCrossTransaction() {
        createMockAccount("acc3", new BigDecimal("20.00"), TRY);
        createMockAccount("acc4", new BigDecimal("10.00"), EUR);
        given().
                when()
                .body(new TransactionRequest("acc4", "acc3", new BigDecimal("2.00")))
                .contentType("application/json")
                .post("/accounts/transaction")
                .then().statusCode(200);
        when()
                .get("/accounts/acc3")
                .then()
                .body("accountBalance", comparesEqualTo(new BigDecimal("30.60")));
        when()
                .get("/accounts/acc4")
                .then()
                .body("accountBalance", comparesEqualTo(new BigDecimal("8.00")));
    }

    @Test
    public void testInvalidTransaction() {
        createMockAccount("acc1", new BigDecimal("1.00"), TRY);
        createMockAccount("acc2", new BigDecimal("2.00"), TRY);
        given().
                when()
                .body(new TransactionRequest("acc5", "acc1", new BigDecimal("2.00")))
                .contentType("application/json")
                .post("/accounts/transaction")
                .then()
                .statusCode(400)
                .body(containsString("IllegalArgumentException"));

    }

    @Test
    public void testInvalidTransactionWithNotEnoughBalance() {
        createMockAccount("acc1", new BigDecimal("1.00"), TRY);
        createMockAccount("acc2", new BigDecimal("1.00"), TRY);
        given().
                when()
                .body(new TransactionRequest("acc1", "acc2", new BigDecimal("2.00")))
                .contentType("application/json")
                .post("/accounts/transaction")
                .then()
                .statusCode(400)
                .body(containsString("Not enough balance"));
        when()
                .get("/accounts/acc1")
                .then()
                .body("accountBalance", comparesEqualTo(new BigDecimal("1.00")));
        when()
                .get("/accounts/acc2")
                .then()
                .body("accountBalance", comparesEqualTo(new BigDecimal("1.00")));
    }

    private void createMockAccount(String accNum, BigDecimal balance, Currency currency) {
        main.require(IAccountRepository.class).saveAccount(new Account(accNum, currency, balance));
    }
}