package revolut;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import revolut.domain.Account;
import revolut.service.Interface.IAccountRepository;
import revolut.service.Interface.IAccountTransactionService;
import revolut.service.Interface.ICurrencyRateSupplier;
import revolut.service.Interface.ITransactionService;
import revolut.service.TransactionService;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static java.math.RoundingMode.HALF_UP;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static revolut.domain.Currency.*;

public class TransactionServiceTest {

    @Mock
    private IAccountRepository accountRepository;
    @Mock
    private ICurrencyRateSupplier currencyRateSupplier;
    private IAccountTransactionService accountTransactionService = (fromAccount, toAccount, biConsumer) -> biConsumer.accept(fromAccount, toAccount);
    private ITransactionService transactionService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        transactionService = new TransactionService(accountRepository, currencyRateSupplier, accountTransactionService);
        when(currencyRateSupplier.getRate(TRY, TRY)).thenReturn(ONE);
        when(currencyRateSupplier.getRate(USD, EUR)).thenReturn(new BigDecimal("0.87"));
    }

    @Test
    public void checkSameCurrencyTransaction() {
        Account acc1 = new Account("acc1", TRY, new BigDecimal("15.00"));
        Account acc2 = new Account("acc2", TRY, new BigDecimal("10.00"));

        when(accountRepository.getAccount("acc1")).thenReturn(of(acc1));
        when(accountRepository.getAccount("acc2")).thenReturn(of(acc2));

        transactionService.makeTransaction("acc1", "acc2", new BigDecimal("5.0"));

        assertEquals(acc1.getAccountBalance(), new BigDecimal(10.00).setScale(2, HALF_UP));
        assertEquals(acc2.getAccountBalance(), new BigDecimal(15.00).setScale(2, HALF_UP));
    }

    @Test
    public void checkCrossCurrencyTransaction() {
        Account acc1 = new Account("acc1", USD, new BigDecimal("15.00"));
        Account acc2 = new Account("acc2", EUR, new BigDecimal("20.00"));

        when(accountRepository.getAccount("acc1")).thenReturn(of(acc1));
        when(accountRepository.getAccount("acc2")).thenReturn(of(acc2));

        transactionService.makeTransaction("acc1", "acc2", new BigDecimal("10"));

        assertEquals(acc1.getAccountBalance(), new BigDecimal(5.00).setScale(2, HALF_UP));
        assertEquals(acc2.getAccountBalance(), new BigDecimal(28.70).setScale(2, HALF_UP));
    }
}